<?php
require_once __DIR__.'/../../helper/init.php';
$pageTitle = "Easy ERP | View Customer";
$sidebarSection = "customer";
$sidebarSubSection = "view";
Util::createCSRFToken();
$errors = "";
if(Session::hasSession('errors'))
{
    $errors = unserialize(Session::getSession('errors'));
    Session::unsetSession('errors');
}
$old = "";
if(Session::hasSession('old'))
{
    $old = Session::getSession('old');
    Session::unsetSession('old');
}

    if(isset($_GET['id'])){
        $id = $_GET['id'];
        
        $row = $di->get('database')->raw("SELECT * FROM customers WHERE id = $id");
        $address = $di->get('database')->raw("SELECT * FROM address WHERE id = (SELECT address_id FROM address_customer WHERE customer_id = $id)");
        // Util::dd($row);
        if($row === false && $address == false)
        {
            $error = Util::db_error();
            Util::dd($error);
        }
    }else{
        Util::redirect("404.html");
    }
   

    
?>
<!DOCTYPE html>
<html lang="en">

<head>
  <?php
  require_once __DIR__ . "/../includes/head-section.php";
  ?>

  <!--PLACE TO ADD YOUR CUSTOM CSS-->

</head>

<body id="page-top">
  <!-- Page Wrapper -->
  <div id="wrapper">
    <?php require_once(__DIR__ . "/../includes/sidebar.php"); ?>
    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
      <!-- Main Content -->
      <div id="content">
        <?php require_once(__DIR__ . "/../includes/navbar.php"); ?>
        <!-- Begin Page Content -->
        <div class="container-fluid">

          <div class="d-sm-flex align-items-center justify-content-between mb-4">
            <h1 class="h3 mb-0 text-gray-800">Customer</h1>
            <a href="<?= BASEPAGES; ?>manage-customer.php"
              class="d-none d-sm-inline-block btn btn-sm btn-primary shadow-sm">
              <i class="fa fa-list-ul fa-sm text-white-75"></i> Manage Customer
            </a>
          </div>

          <div class="row">
            <div class="col-lg-12">

              <!-- Basic Card Example -->
              <div class="card shadow mb-4">
                <div class="card-header py-3">
                  <h6 class="m-0 font-weight-bold text-primary">View Customer</h6>
                </div>
                <div class="card-body">
                  <div class="col-md-12">
                    <form action="<?=BASEURL;?>helper/routing.php" method="POST" id="edit-customer">
                      <input type="hidden" name="csrf_token" value="<?=Session::getSession('csrf_token');?>">
                      <!-- FORM GROUP -->
                      <div class="form-group">
                        <div class="row">
                          <div class="col-md-6">
                            <div class="row">
                              <div class="col-md-4">
                                <label for="first_name">First Name</label>
                              </div>
                              <div class="col-md-8">
                                <input type="text" readonly value="<?=$row[0]->first_name?>">
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="row">
                              <div class="col-md-4">
                                 <label for="last_name">Last Name</label>
                              </div>
                              <div class="col-md-8">
                              <input type="text" readonly value="<?=$row[0]->last_name?>">
                              </div>
                            </div>
                          </div>
                        </div>

                        <br>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="row">
                              <div class="col-md-4">
                                <label for="gst_no">GST Number</label>
                              </div>
                              <div class="col-md-8">
                                <input type="text" readonly value="<?=$row[0]->gst_no?>">
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="row">
                              <div class="col-md-4">
                                 <label for="phone_no">Phone Number</label>
                              </div>
                              <div class="col-md-8">
                              <input type="text" readonly value="<?=$row[0]->phone_no?>">
                              </div>
                            </div>
                          </div>
                        </div>

                        <br>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="row">
                              <div class="col-md-4">
                                <label for="email_id">Email ID</label>
                              </div>
                              <div class="col-md-8">
                                <input type="text" readonly value="<?=$row[0]->email_id?>">
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="row">
                              <div class="col-md-4">
                                 <label for="gender">Gender</label>
                              </div>
                              <div class="col-md-8">
                              <input type="text" readonly value="<?=$row[0]->gender?>">
                              </div>
                            </div>
                          </div>
                        </div>
                        
                        <br>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="row">
                              <div class="col-md-4">
                                <label for="block_no">Block No</label>
                              </div>
                              <div class="col-md-8">
                                <input type="text" readonly value="<?=$address[0]->block_no?>">
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="row">
                              <div class="col-md-4">
                                 <label for="street">Street</label>
                              </div>
                              <div class="col-md-8">
                              <input type="text" readonly value="<?=$address[0]->street?>">
                              </div>
                            </div>
                          </div>
                        </div>

                        <br>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="row">
                              <div class="col-md-4">
                                <label for="city">City</label>
                              </div>
                              <div class="col-md-8">
                                <input type="text" readonly value="<?=$address[0]->city?>">
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                            <div class="row">
                              <div class="col-md-4">
                                 <label for="pincode">Pincode</label>
                              </div>
                              <div class="col-md-8">
                              <input type="text" readonly value="<?=$address[0]->pincode?>">
                              </div>
                            </div>
                          </div>
                        </div>

                        <br>
                        <div class="row">
                          <div class="col-md-6">
                            <div class="row">
                              <div class="col-md-4">
                                <label for="state">State</label>
                              </div>
                              <div class="col-md-8">
                                <input type="text" readonly value="<?=$address[0]->state?>">
                              </div>
                            </div>
                          </div>
                          <div class="col-md-6">
                          <div class="row">
                              <div class="col-md-4">
                                <label for="town">Town</label>
                              </div>
                              <div class="col-md-8">
                                <input type="text" readonly value="<?=$address[0]->town?>">
                              </div>
                            </div>
                          </div>
                        </div>

                        <br>
                        <div class="row">
                          <div class="col-md-6">
                          <div class="row">
                              <div class="col-md-4">
                                 <label for="country">Country</label>
                              </div>
                              <div class="col-md-8">
                              <input type="text" readonly value="<?=$address[0]->country?>">
                              </div>
                            </div>
                            
                          </div>
                          <div class="col-md-6">
                            
                        </div>
                        
                      </div>
                      <br>
                      <!--/FORM GROUP-->
                      <a href="<?=BASEPAGES?>edit-customer.php"><button type="submit" class="btn btn-primary"
                          name="view_customer" value="viewCustomer"><i class="fa fa-pen"></i> Edit</button></a>
                      <button type="submit" class="btn btn-danger" name="view_customer" data-target="#deleteModal"
                        value="viewCustomer"><i class="fas fa-trash"></i> Delete</button>
                        <a href="<?=BASEPAGES?>manage-customer.php"><button type="submit" class="btn btn-success"
                          name="view_customer" value="viewCustomer"><i class="fa fa-pen"></i> Back</button></a>
                        </div>

                      </form>
                  </div>
                  </div>

                <!-- /.container-fluid -->
              </div>
              <!-- End of Main Content -->
              <!-- Footer -->
              <?php require_once(__DIR__ . "/../includes/footer.php"); ?>
              <!-- End of Footer -->
            </div>
            <!-- End of Content Wrapper -->
          </div>
          <!-- End of Page Wrapper -->
          <?php
  require_once(__DIR__ . "/../includes/scroll-to-top.php");
  ?>
          <?php require_once(__DIR__ . "/../includes/core-scripts.php"); ?>

          <!--PAGE LEVEL SCRIPTS-->
          <?php require_once(__DIR__ . "/../includes/page-level/customer/view-customer-scripts.php");?>
</body>

</html>