/**
 * scripts.js
 * Contains Script for basic static website named "White Graphics"
 */
$(window).on('load', function(){
    $("#preloader").delay(500).fadeOut('slow');
//    setInterval is delay in js
//    fadeout animation 
});

$(function () {
   $("#team-members").owlCarousel({
      items: 2,
       autoplay: true, //ghumta rahega iske wajah se
       smartSpeed: 700, //How many Millisecond
       loop: true, //peche se agge loop ke wajah se
       autoplayHoverPause: true, //jitne der hover hai utni der pause rahega
       dots: false, //by default true hota hai dots wo slideshow ke neeceh ata hai
       nav: true, //left right navigation button true
       navText: ['<i class="lni-chevron-left-circle"></i>','<i class="lni-chevron-right-circle"></i> '],
       responsive: {
           //0 or up wala width
           0: {
               items: 1 //no. of items to be shown at the width it will show 1 item at width 0 to 480
           }, 
           480: {
               items: 2 //it will show 2 items at width 480 above
           }
       }
   });
    
    $("#our-clients").owlCarousel({
      items: 6,
       autoplay: false, //ghumta rahega iske wajah se
       smartSpeed: 700, //How many Millisecond
       loop: true, //peche se agge loop ke wajah se
       autoplayHoverPause: true, //jitne der hover hai utni der pause rahega
       dots: false, //by default true hota hai dots wo slideshow ke neeceh ata hai
       nav: true, //left right navigation button true
       navText: ['<i class="lni-chevron-left-circle"></i>','<i class="lni-chevron-right-circle"></i> '],
       responsive: {
           //0 or up wala width
           0: {
               items: 3 //no. of items to be shown at the width it will show 1 item at width 0 to 480
           }, 
           720: {
               items: 6 //it will show 2 items at width 480 above
           }
       }
   });
    
    $("#testimonial-blogs").owlCarousel({
       items: 3,
       autoplay: false, //ghumta rahega iske wajah se
       smartSpeed: 700, //How many Millisecond
       loop: true, //peche se agge loop ke wajah se
       autoplayHoverPause: true, //jitne der hover hai utni der pause rahega
       dots: false, //by default true hota hai dots wo slideshow ke neeceh ata hai
       nav: true, //left right navigation button true
       navText: ['<i class="lni-chevron-left-circle"></i>','<i class="lni-chevron-right-circle"></i> '],
       responsive: {
           //0 or up wala width
           0: {
               items: 1 //no. of items to be shown at the width it will show 1 item at width 0 to 480
           }, 
           720: {
               items: 1 //it will show 2 items at width 480 above
           }
       }
   });
    
    
    $('#progress-elements').waypoint(function () {
       $('.progress-bar').each(function () {
          $(this).animate({
              width: $(this).attr('aria-valuenow') + "%"
          }, 800); 
       });
        this.destroy();
    },{
        offset: 'bottom-in-view'
//        jidr div khatam ho raha hai waha pe ye animation start hoga
    }
);
    
    //SERVICES SECTION
    $("#services-tabs").responsiveTabs({
        animation: 'slide'
    });
    
    $("#isotope-container").isotope({});
    $('#isotope-filters').on('click', 'button', function () {
        var filterValue = $(this).attr('data-filter');
        $("#isotope-container").isotope({
            filter: filterValue
        });

        $("#isotope-filters").find(".active").removeClass("active");
        $(this).addClass("active");
    });

    $("#portfolio-wrapper").magnificPopup({
        delegate: 'a', // child items selector, by clicking on it popup will open
        type: 'image',
        gallery: {
            enabled: true
        },
        zoom: {
            enabled: true
        },
        overflowY: 'scroll',
        callbacks: {
            open: function(){
                $("html").css('margin-right', '0');
            }
        }
    });
    
     $('.counter').counterUp({
                delay: 10,
                time: 1000
            });
    
    $(window).scroll(function(){
        showHideNav(); 
    });
    
    function showHideNav(){
        if($(window).scrollTop() > 50){
            $("nav").addClass("scrolled-navbar green-nav-top");
            $(".navbar-brand img").attr('src', 'img/logo/logo-dark.png');
            $(".btn-back-to-top").slideDown();
        }else{
            $("nav").removeClass("scrolled-navbar green-nav-top");
            $(".navbar-brand img").attr('src', 'img/logo/logo.png');
            $(".btn-back-to-top").slideUp();
        }
    }
    
    $("#mobile-nav-open-btn").click(function(){
        $("#mobile-nav").css('height', '100%');
        $(".mobile-nav-content").css("display", "block");
        $("#mobile-nav-close-btn").css("display", "block");
    });
    
    $("#mobile-nav-close-btn").click(function(){
        $("#mobile-nav").css('height', '0%');
        $(".mobile-nav-content").css("display", "none");
        $("#mobile-nav-close-btn").css("display", "none");
    });
    
    $(".smooth-scroll").click(function(e){
        e.preventDefault();
        var section_id = $(this).attr("href");
        $("html, body").animate({
            scrollTop: $(section_id).offset().top - 50
        }, 1250, "easeInOutExpo");
    });
    
    /***************************************************
                GOOGLE MAP
    ***************************************************/
    
    /*******************************************************************************************
        LINK FOR MAPS: https://developers.google.com/maps/documentation/embed/guide
    *********************************************************************************************/
    var addressString = "<h2>White Graphics</h2> <p>301 Evergreen CHS.,<br> Airoli, Maharashtra, India.</p>";
    var myLatLng = {
        lat: 19.145217,
        lng: 72.989140
    };
    
    var map = new google.maps.Map(document.getElementById("map"), {
        zoom: 13,
        center: myLatLng
    });
    
    var marker = new google.maps.Marker({
       position: myLatLng,
        map: map,
        title: 'Click to see the address!'
    });
    
    var infoWindow = new google.maps.InfoWindow({
       content: addressString /*We can design whole card*/
    });
    
    marker.addListener('click', function(){ //addListener is same as addEventListener in js
       infoWindow.open(map, marker); 
    });
   
});

